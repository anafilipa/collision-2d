#include <vector>
using namespace std;
typedef
struct pressurecomp {       //momentum transfer at the walls
  vector<double> p;          //vector with momentum transferer
  double ti;
  double sum;               
  double mean;
}pc;


typedef
struct meanfreepathcomp{                 
  int lastcollflag;           //checks if the collision was with
                              //a wall or particle
  double lastcolltime;        //time passed since the last coll
                              //with a particle
  vector<double> mfp;         //vector with distances between coll
  double mfpmean;
}mfpc;

typedef
struct temperaturecomp{
  
  vector<double> meanvelocity;  //mean velocity
  double dvel;                  //bin size
  int bins;                     //number of bins
  int maxelement;               //max "y" element of hystogram
  double maxvelocity;           //max "x" element of hystogram
  double temp;                  //fitted temperature
  double etemp;                 //error of fit
  double tempindex;

  
}tmpc;
  


  
