#include<iostream>
#include<cstdlib>
#include<fstream>
#include"FITTER.h"

using namespace ROOT::Minuit2;
using namespace std;

int main() {

// SAO OS MEUS PONTOS
  double *x=new double[8];
  double *y=new double[8];
  double *err=new double[8];
  for(int i=0;i<8;++i){
    err[i]=abs(0.07*sin(i+.01)); 
    x[i]=i;y[i]=i*i*i-2*i+3;
  }
//


  mm j;// ESCREVES ISTO

  j.GetPoints(8,x,y,err);// AQUI DAS OS PONTOS AO FITTER, SE NAO CONSEGUES PERCEBER SOZINHA ISTO MERECES MORRER

  // ISTO SAO OS PARAMETROS DE AJUSTES, METES TANTOS QTS QUEIRAS, A PRIMEIRA ENTRADA NUMERICA E A TENTATIVA INICIAL E A SEGUNDA O ERRO QUE ESPERAS QUE TENHA. EM PARTIDA DEIXA ASSIM, NAO DEVES TER PROBLEMAS COM ISTO.
  MnUserParameters upar;
  upar.Add("m",1,.01);
  upar.Add("b",1,.01);
  // PARA USARES OS VALORES DISTO A MEIO DO PROGRAMA, USA OS SEGUINTES COMANDOS:

  // upar.Value(i)-> comeca no zero, acho
  // upar.Error(i)

  MnMigrad migrad(j,upar);
  FunctionMinimum min=migrad();// ESTE E O COMANDO PARA FITAR

  cout<<min<<endl;// ISTO IMPRIME O RESULTADO NA LINHA DE COMANDOS

}

