#define pi 3.141592653589793238463
#define kb 1.38064852e23 

double userfunction1(double x, const std::vector<double>& par){

  double m=par[0], b=par[1];
  return m*x+b;

}

double userfunction2(double x, const std::vector<double>& par){

 double kbT = par[0];
double c1 = par[1];		
double c2 = par[2];
  double m=par[3], b=par[4];

return c1/kbT*(x-c2)*exp(-(x-c2)*(x-c2)/2./kbT)+m*x+b;

}
