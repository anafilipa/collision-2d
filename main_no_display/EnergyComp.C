#include <vector>
using namespace std;
#include "EnergyComp.h"

double EnergyComp(vector <double> vx, vector <double> vy, int particlenumb){
  double velocitysum = 0;
  double velocitymean = 0;

  for( int j = 0; j<particlenumb; j++) velocitysum+=vx[j]*vx[j]+vy[j]*vy[j];
  velocitymean = velocitysum / double(particlenumb);

  return velocitysum/2;
  
};
