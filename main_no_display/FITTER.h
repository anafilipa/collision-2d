#ifndef __am__
#define __am__


#include "Minuit2/FCNGradientBase.h"
#include "Minuit2/MnPrint.h"
#include "Minuit2/MnUserParameters.h"
#include "Minuit2/MnMigrad.h"
#include "Minuit2/FunctionMinimum.h"
#include<cmath>
#include "userfunction.h"
#include<iostream>
using namespace std;

// NAO TENS REALMENTE QUE MEXER NISTO. VAI A TUA VIDA. CARALHO

namespace ROOT{

  namespace Minuit2 {

class mm : public FCNBase {

 public:

  mm(){};
  ~mm(){};



  //_____________________________________________________________
  void GetPoints(int N, double *xx, double *yy, double *errr){

    size=N;
    x=new double[size];
    y=new double[size];
    err=new double[size];

    for(int i=0;i<size;++i){

      x[i]=xx[i];
      y[i]=yy[i];
      err[i]=errr[i];
    }

  }

  //_____________________________________________________________
  double operator()(const std::vector<double>& par) const {

    double chi=0;
    for(int i=0;i<size;++i){

      double res=userfunction2(x[i],par);
      chi+=pow( (res-y[i])/err[i] , 2 );

    }

    return chi;

  }

  //_____________________________________________________________
  double Up() const {return 1.;}

 private:

  int size;
  double *x, *y, *err;

};}}

#endif

