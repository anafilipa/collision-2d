#include <vector>
using namespace std;
typedef
struct pressurecomp {       //momentum transfer at the walls
  int i ;                   //current index
  vector<double> p;          //vector with momentum transferer
  double ti;
  double mean;
}pc;


typedef
struct meanfreepathcomp{
int lastcollflag;           //checks if the collision was with
                            //a wall or particle
double lastcolltime;        //time passed since the last coll
                            //with a particle
vector<double> mfp;
double mfpmean;
}mfpc;
